import cherrypy
import json
from glob import glob
# from html import HTML
# from dominate.tags import *

# CONFIG
strPathToImages = "/Users/belthesar/Dropbox/gifs"
listFileType = ["gif"]
intServerPort = "8080" #does not work yet, but does bind to this port
# END CONFIG

def setup():
	cherryp.config.update({'server.socket_port': intServerPort})


def getDirList(path, fileTypes):
	listFiles = []
	for fileType in fileTypes:
		filesForType = glob(path + "/*." + fileType)
		listFiles = listFiles + filesForType
	return listFiles

class imgRotator():
	@cherrypy.expose
	def index(self):
		return json.dumps({"images": getDirList(strPathToImages, listFileType)})


cherrypy.quickstart(imgRotator(), '/')
